package crawler;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;


public class PageRepository {
	public static PriorityQueue<Page> visitUrlQueue=new PriorityQueue<Page>(1000,
			new Comparator<Page>() {
          		public int compare(Page page1,Page page2) {
		            double score1 = page1.getPageScore();
		            double score2 = page2.getPageScore();
		            return (int) (score2 - score1);
          		};	
	} );  
	static ArrayList<Page> seedPages=new ArrayList<Page>();
}
