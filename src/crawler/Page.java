package crawler;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class Page {
	
	URL pageUrl;
	HashMap<Keyword,Integer> pageKeywords;
	String pageContent;
	double pageScore;
	Page parentPage;
	ArrayList<Page> childPages=new ArrayList<Page>(); 
	String pageName;
	public Page(URL url,Page page,double score) {
		pageUrl=url;
		parentPage=page;
		pageScore=score;
	}
	
	public Page() {
		
	}
	public Page(double score) {
		pageScore=score;
	}
	
	public Page(URL url,String name,Page page,double score) {
		pageUrl=url;
		parentPage=page;
		pageName=name;
		pageScore=score;
	}
	
	public URL getPageUrl() {
		return pageUrl;
	}
	
	public HashMap<Keyword,Integer> getPageKeywords() {
		return pageKeywords;
	}
	
	public String getPageContent() {
		return pageContent;
	}
	
	public double getPageScore() {
		return pageScore;
	}
	
	public Page getParentPage() {
		return parentPage;
	}
	
	public ArrayList<Page> getChildPages() {
		return childPages;
	}
	
	public String getPageName() {
		return pageName;
	}
	
	public void setPageUrl(URL url) {
		pageUrl=url;
	}
	
	public void setPageKeywords(HashMap<Keyword,Integer> keywords) {
		pageKeywords=keywords;
	}
	
	public void setPageContent(String content) {
		pageContent=content;
	}
	
	public void setPageScore(double score) {
		pageScore=score;
	}
	
	public void setParentPage(Page page) {
		 parentPage=page;
	}
	
	public void setChildPages(ArrayList<Page> subPage) {
		childPages=subPage;
	}
	
	public void setChildPage(Page subPage) {
		childPages.add(subPage);
	}
	
	public void setPageName(String name) {
		pageName=name;
	}
	
	

}
