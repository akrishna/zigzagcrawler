package crawler;

import java.net.MalformedURLException;
import java.net.URL;

public class UrlNormalizer {
	URL pageUrl;
	
	
	public URL doNormalize(URL url) {
		pageUrl=url;
		try {
			return new URL(pageUrl.toString().toLowerCase());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			return null;
		}
		
	}
	
	public URL makeUrlComplete(String url,Page page){
		String absoluteUrl;
		if(!url.contains("http") && !url.contains("mailto") ) {
			absoluteUrl=page.getPageUrl()+"/"+url;
		}
		else 
			absoluteUrl=url;
		try {
			URL checkUrl=new URL(absoluteUrl);
			return checkUrl;
		}
		catch (MalformedURLException e) {
				return null;
		}

	}
}
