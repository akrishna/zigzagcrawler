package crawler;
import java.util.ArrayList;


public class Keyword {
	
	private double keywordValue;
	private String keywordName;
	private ArrayList<Keyword> synonymes;
	
	public Keyword(String name){
		keywordName=name;
	}
	public double getKeywordValue() {
		return keywordValue;
	}
	
	public String getKeywordName() {
		return keywordName;
	}
	
	public ArrayList<Keyword> getKeywordSynonymes() {
		return synonymes;
	}
	
	public void setKeywordValue(double value) {
		keywordValue=value;
	}
	
	public void setKeywordName(String name) {
		keywordName=name;
	}
	
	public void setKeywordSynonymes(ArrayList<Keyword> keywords) {
		synonymes=keywords;
	}
}
