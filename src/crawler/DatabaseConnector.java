package crawler;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseConnector {

		public void updateUrlVIsit(URL url) {
			// declare a connection by using Connection interface 
			Connection connection = null;
			/* Create string of connection url within specified format with machine 
			name, port number and database name. Here machine name id localhost 
			and database name is mahendra. */
			String connectionURL = "jdbc:mysql://10.8.0.2:3306/mycrawler";
			// Declare prepare statement.
			PreparedStatement psmnt = null;
			try {
			// Load JDBC driver "com.mysql.jdbc.Driver"
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			/* Create a connection by using getConnection() method that takes 
			parameters of string type connection url, user name and password to 
			connect to database. */
			connection = DriverManager.getConnection(connectionURL,DatabaseConfig.userName,DatabaseConfig.password);
			// create a file object for image by specifying full path of image as parameter.

			/* prepareStatement() is used for create statement object that is 
			used for sending sql statements to the specified database. */
			psmnt = connection.prepareStatement
			("insert into urlRepository(URL) "+ "values(?)");
			psmnt.setString(1,url.toString());
			/* executeUpdate() method execute specified sql query. Here this query 
			insert data and image from specified address. */ 
			int s = psmnt.executeUpdate();
			if(s>0) {
			System.out.println("Uploaded successfully !");
			}
			else {
			System.out.println("unsucessfull to upload image.");
			}
			}
			// catch if found any exception during rum time.
			catch (Exception ex) {
			System.out.println("Found some error : "+ex);
			}
			finally {
			// close all the connections.
			try {	
				connection.close();
			    psmnt.close();
			}catch(SQLException e) {
				System.out.println("connection closed");
			}
		}
	}
		
		public void updateEmailVIsit(URL url) {
			// declare a connection by using Connection interface 
			Connection connection = null;
			/* Create string of connection url within specified format with machine 
			name, port number and database name. Here machine name id localhost 
			and database name is mahendra. */
			String connectionURL = "jdbc:mysql://10.8.0.2:3306/mycrawler";
			// Declare prepare statement.
			PreparedStatement psmnt = null;
			try {
			// Load JDBC driver "com.mysql.jdbc.Driver"
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			/* Create a connection by using getConnection() method that takes 
			parameters of string type connection url, user name and password to 
			connect to database. */
			connection = DriverManager.getConnection(connectionURL,DatabaseConfig.userName,DatabaseConfig.password);
			// create a file object for image by specifying full path of image as parameter.

			/* prepareStatement() is used for create statement object that is 
			used for sending sql statements to the specified database. */
			psmnt = connection.prepareStatement
			("insert into emailRepository(email) "+ "values(?)");
			psmnt.setString(1,url.toString());
			/* executeUpdate() method execute specified sql query. Here this query 
			insert data and image from specified address. */ 
			int s = psmnt.executeUpdate();
			if(s>0) {
			System.out.println("Uploaded successfully !");
			}
			else {
			System.out.println("unsucessfull to upload image.");
			}
			}
			// catch if found any exception during rum time.
			catch (Exception ex) {
			System.out.println("Found some error : "+ex);
			}
			finally {
			// close all the connections.
			try {	
				connection.close();
			    psmnt.close();
			}catch(SQLException e) {
				System.out.println("connection closed");
			}
		}
	}
}
