package crawler;
import java.net.Authenticator;
import java.net.PasswordAuthentication;


public class Proxy {

	public static void initializeProxy() {
        final String authUser = "298397";
        final String authPassword = "dolly";

        System.setProperty("http.proxyHost", "10.1.1.19");
        System.setProperty("http.proxyPort", "80");
        System.setProperty("http.proxyUser", authUser);
        System.setProperty("http.proxyPassword", authPassword);

        Authenticator.setDefault(
        new Authenticator() {
        @Override
            public PasswordAuthentication getPasswordAuthentication() {
            	return new PasswordAuthentication(authUser, authPassword.toCharArray());
            }
        }
        );
    }
	
}
