package crawler;



import java.net.URL;
import java.util.HashMap;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class LinkExtracter {
	Document doc=null;
	HashMap<Page,Integer> childPagesValue;
	SyntacticLinkScorer syntacticScore=new SyntacticLinkScorer();
	SemanticLinkScorer semanticScore=new SemanticLinkScorer();;
	Page page;
	UrlNormalizer normalizer=new UrlNormalizer();
	double SyntacticLinkScore,SemanticLinkScore;
	
	public boolean extractLink(Page pageToExtract) {
	try {	
		System.out.println("in the link extracter");
		doc = Jsoup.parse(pageToExtract.getPageContent());
	}catch(NullPointerException e) {
		System.err.println("unable to get links from "+pageToExtract.getPageUrl());
		return false;
		
	}
		Elements links = doc.getElementsByTag("a");
		System.out.println(links.size());
		for (Element link : links) {
		  //try {
			  String linkHref = link.attr("href");
			  String linkText = link.text();//linkText will be used as page name
			  SyntacticLinkScore=syntacticScore.getSyntacticValue(link);
			  SemanticLinkScore=semanticScore.getSemanticValue(link);
			  URL pageUrl=normalizer.makeUrlComplete(linkHref,pageToExtract);
			  if(pageUrl==null) {
				  continue;
			  }
			  else {
				  pageUrl=normalizer.doNormalize(pageUrl);
				  if(!UrlRepository.visitedUrlList.contains(pageUrl)) {
					  System.out.println(linkHref+"--->"+linkText);
					  //page score is equally devided in its child pages
					  //it will be distributed such that child page will have score 
					  // higher than any of uncle page only in rare conditions
					  page=new Page(pageUrl,linkText,pageToExtract,(SyntacticLinkScore+SemanticLinkScore)/2+
							  pageToExtract.getPageScore()/links.size()
					  							);
					  pageToExtract.setChildPage(page);
					  PageRepository.visitUrlQueue.add(page);
					  UrlRepository.visitedUrlList.add(pageUrl);
				  }
				  else {
					  continue;
				  }
			  }
		 // }
		}
	
	return true;
		
		
	}
}
