package crawler;

import org.jsoup.nodes.Element;
//it will have stuff to score link based on syntactic behaviour
//<b>,<i> etc and other attributes of anchor tag(name,value,onclick)
public class SyntacticLinkScorer {
	String linkHtml;
	String linkText;
	double textLengthScore=0;
	double linktargetScore=0;
	double tagValueScore=0;
	double nameLengthScore=0;
	public double getSyntacticValue(Element link) {
		linkHtml=link.html();
		linkText=link.text();
		// we take the integer division from a expected link average length(10)
		//it can be stored for each and then predict?
		textLengthScore=min((linkText.length()/10),10);
		nameLengthScore=min((link.attr("name").length()/4),10);
		if(link.attr("target")=="_blank") {
			linktargetScore=0;
		}
		else {
			linktargetScore=10;
		}
		if(linkHtml.contains("<h1>")){
			tagValueScore=7;
		}
		if(linkHtml.contains("<h2>")){
			tagValueScore=6;
		}
		if(linkHtml.contains("<div>")){
			tagValueScore=5;
		}
		else if(linkHtml.contains("<u>")){
			tagValueScore=4;
		}
		else if(linkHtml.contains("<b>")||linkHtml.contains("<strong>")){
			tagValueScore=3;
		}
		else if(linkHtml.contains("<i>")){
			tagValueScore=2;
		}
		else if(linkHtml.contains("<img>")||linkHtml.contains("#")){
			tagValueScore=0;
		}
		else {
			tagValueScore=1;
		}
		return linktargetScore+tagValueScore+textLengthScore+nameLengthScore;
		
	}
	
	public double min(double a,double b) {
		if(a>b) {
			return b;
		}
		else return a;
	}
}
