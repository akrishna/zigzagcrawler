package crawler;

public class ContentExtracter {
	public void extractContent(Page page) {
		double pageContribution;
		//use ontology somehow to understand content in more accurate way
		pageContribution=KeywordRepository.setCoverKeywords.size()/KeywordRepository.PageKeyword.get(page).size();
		page.setPageScore(page.getPageScore()+10*pageContribution);
	}
}
