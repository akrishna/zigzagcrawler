package crawler;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;




public class SeedUrl {
	UrlNormalizer normalizer=new UrlNormalizer();
	SetCover setCover=new SetCover();
	KeywordExtracter ke=new KeywordExtracter();
	LinkExtracter le=new LinkExtracter();
	ContentExtracter ce=new ContentExtracter();
	ArrayList<String> seedUrlList;
	DatabaseConnector dc=new DatabaseConnector();

	public SeedUrl(ArrayList<String> urlList){
		seedUrlList=urlList;
	}
	public void startCrawling()  {
		InputStream in;
		InputStreamReader isr;
		BufferedReader reader;
		Page page;
		URL url = null;
		HttpURLConnection  connection;
		String inputLine;
		String result="";
		Proxy.initializeProxy();
		for(String urlString:seedUrlList) {
			try {
				url=normalizer.doNormalize(new URL(urlString));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}
			if(url!=null){
				page=new Page(url,null,40);
				PageRepository.visitUrlQueue.add(page);
				UrlRepository.visitedUrlList.add(url);
				PageRepository.seedPages.add(page);
			}
			else 
				continue;
		}

		
		while(!PageRepository.visitUrlQueue.isEmpty()) {
			//in=null;
			//isr=null;
		    //result = "aa";
			connection=null;
			page=PageRepository.visitUrlQueue.remove();
			try {
				System.out.println("connecting......"+page.pageUrl);
				url=page.pageUrl;
				dc.updateUrlVIsit(url);
				connection = (HttpURLConnection)url.openConnection();
				//connection.setRequestMethod("HEAD");
				connection.setConnectTimeout(10000);
				connection.setReadTimeout(10000);
			}catch(SocketTimeoutException e) {
				System.err.println(page.pageUrl+ "-takes a long time to download");
				continue;
			}catch (MalformedURLException e) {
				System.err.println(page.pageUrl+ "-url problem");
				continue;
			}catch (IOException e) {
				System.err.println(page.pageUrl+ "-could not connect due to I/O");
				continue;
			}catch(ClassCastException e) {
				dc.updateUrlVIsit(url);
				System.err.println(page.pageUrl+ "-that was a a mail to link");
				continue;
			}
			try {
				System.out.println("checking status code......");
				if(connection.getResponseCode()==HttpURLConnection.HTTP_OK) {
					
					
					try {
						System.out.println("getting input stream......"+connection.getResponseCode());
						in=connection.getInputStream();
					}catch(UnknownHostException e) {
						System.err.println(page.pageUrl+ "-dns not resoving for this");
						continue;
					}catch(IOException e) {
						System.err.println(page.pageUrl+ "-no inputStream");
						continue;
					}
					isr = new InputStreamReader(in);
					reader = new BufferedReader(isr);
					try {
						//System.out.println(isr.);
						while( (inputLine = reader.readLine()) != null ) {
							result += inputLine;
						}
					}catch(IOException e) {
						System.err.println(page.pageUrl+ "-no inputStream");
						continue;
					}
					System.out.println("starts from here");
					System.out.println(result);
					System.out.println("ankur krishna gautam");
					page.setPageContent(result);
					System.out.println("extracting keywords");
					ke.extractKeyword(page);
					//make set cover from all seed url keywords or from
					//all urls till the moment
					setCover.makeSetCover();
					System.out.println("extracting content");
					ce.extractContent(page);
					//call keyWordExtracter before linkExtracter
					if(!le.extractLink(page)) {
						System.err.println(page.pageUrl+ "-link does not resolve");
						continue;
					}
					else {
						connection.disconnect();
						System.out.println("successfulley extracted");
						continue;
					}
					
 
				}
				else {
					System.err.println(page.pageUrl+ "-Bad HTTP response code");
					continue;
				}
			}catch(UnknownHostException e) {
				System.err.println(page.pageUrl+ "-unable to determine HTTP response code");
				continue;
			}catch(IOException e) {
				System.err.println(page.pageUrl+ "-IO problem");
				continue;
			}
			
		}
		System.out.println("frontier is empty");
	}
	
}
